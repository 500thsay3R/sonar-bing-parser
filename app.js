/* eslint-env: node, es6 */
/* eslint strict: 0 */
/* eslint no-console: 0 */
/* eslint arrow-body-style: 0 */
/* eslint no-param-reassign: 0 */
/* eslint no-underscore-dangle: 0 */
/* eslint no-return-assign: 0 */
'use strict';

const url = require('url');
const _ = require('lodash');
const cheerio = require('cheerio');
const request = require('request');
const querystring = require('querystring');
const winston = require('winston');

// set up logging preferences
const logger = new (winston.Logger)({
  transports: [
    new (winston.transports.Console)({
      prettyPrint: true,
      level: process.env.NODE_ENV,
    }),
  ],
});

const countries = require('./lib/country-settings.json');

/** Class Representing Bing Search request. */
class BingSearch {
  /**
   * Create a search request.
   * @param {Object}    params                - The search parameters.
   * @param {!String}   params.query          - The search key.
   * @param {?String}   [params.country=null] - The country to get results for.
   * @param {?String}   [params.period=y]     - The search timeframe.
   * @param {?String}   [params.sort=rel]     - The sort order.
   * @param {String[]}  [params.inc=[]]       - The words every web-page must contain.
   * @param {String[]}  [params.exc=[]]       - The words web-pages should not contain.
   * @param {Number}    [params.limit=500]    - Defines # of resultant web-pages to retrieve.
   *
   * @property {String} NEXT_TEXT - defines Next-button text.
   */
  constructor(params) {
    const { country = null, period = 'y', sort = 'rel', exc = [], inc = [], limit = 150 } = params;

    this.query = params.query;
    this.country = country;
    this.period = period;
    this.sort = sort;
    this.exclusions = exc;
    this.inclusions = inc;
    this.pages = Math.floor(limit / 10);
    this.results = new Set();
  }

  /**
   * Form up body of URL request.
   * @param {Number} [since=0] - Paging parameter.
   * @returns {String} - The well-formed Bing Search request URL.
   *
   * @private
   */
  _formRequest(since = 0) {
    const requestOptions = {
      q: this.query,
      first: 1 + since,
    };

    // if the daily scan is being occurred impound 24h filtration
    if (this.period === 'd') {
      requestOptions.filter = 'ex1:"ez1"';
    }

    // make Bing return country-specific results by using market parameter
    if (this.country && countries[this.country]) {
      requestOptions.market = countries[this.country].market[0];
    }

    // enhance search key by adding plus & minus-words
    if (this.exclusions.length) {
      requestOptions.q += ` -${this.exclusions.join(' -')}`;
    }
    if (this.inclusions.length) {
      requestOptions.q += ` +${this.inclusions.join(' +')}`;
    }

    return url.format({
      protocol: 'https:',
      host: 'bing.com',
      pathname: '/search',
      search: querystring.stringify(requestOptions),
    });
  }

  /**
   * Retrieves Bing results.
   * @param {String} urlRequest - The well-formed URL to connect to.
   * @returns {Promise.<String>} - The page contents.
   * @private
   */
  _performRequest(urlRequest) {
    return new Promise((resolve, reject) => {
      const requestOptions = {
        url: url.format(urlRequest),
        timeout: 15000,
        rejectUnauthorized: false,  // prevents unauthorized connection from being dropped
      };

      request(requestOptions, (error, response) => {
        if (!error && response && response.statusCode === 200) {
          logger.debug(`@BING :: Successfully retrieved data from ${urlRequest}`);

          resolve(response.body.toString());
        } else {
          logger.warn(`@BING :: [${(response) ? response.statusCode : 'ERR'}] Failed to ` +
            `retrieve data from ${urlRequest} -> [${error || 'UNRECOGNIZED ERROR'}].`);

          // TODO: distinguish the exact content of Bing ban message
          // handle potential bans
          if (response && response.body.indexOf('traffic')) {
            logger.error('@BING :: Bing ban confirmed. Requests are temporarily forbidden!');

            reject(new Error('Bing ban'));
          }

          reject(new Error('Bing connection failure'));
        }
      });
    });
  }

  /**
   * Parses data from each Bing results page.
   *
   * @param {String} pageContents - HTML contents of Bing results page.
   * @param {!Number} pageNumber - The number of page that is currently being processed.
   *
   * @returns {Promise.<Object[]>} - A bunch of resultant objectsEach of them includes:
   *          { url: {String}, title: {String}, description: {String}, index: {Number}}.
   * @private
   */
  _processPage(pageContents, pageNumber) {
    const $ = cheerio.load(pageContents);

    // remove extra whitespaces & newline characters
    function prettyPrint(text) {
      return text.replace(/\n/g, ' ').replace(/\s+/g, ' ');
    }

    // analyse each and every resultant link element
    function analyze(elem, pageNum, position) {
      return new Promise((resolve) => {
        // distinguish links and their textual description
        let text = $(elem).find('div.b_snippet p');
        if (!text.length) {
          text = $(elem).find('div.b_caption p');
        }
        const link = $(elem).find('h2 a');

        const searchItem = {
          index: pageNum * 10 + position,
          url: $(link).attr('href'),
          title: prettyPrint($(link).text()),
          description: prettyPrint($(text).text()),
        };

        // omit empty elements
        resolve(searchItem.url ? searchItem : null);
      });
    }

    // process all the result elements in parallel
    const resultElements = $('li.b_algo');

    const tasks = _.map(resultElements, (elem, index) => analyze(elem, pageNumber, ++index));

    // check if the next page of results does indeed exist
    const moreToCome = !!($('a.sb_pagN div').text());

    return Promise.all(tasks)
      .then(results => {
        return { results: _.compact(results), moreToCome };
      });
  }

  /**
   * Performs consecutive results extraction & processing
   * @param {!Number} currentPage   -   Current page of Bing Search results
   * @param {!Number} since         -   Paging step.
   * @returns {Promise.<Object[]>}  -   Retrieved results.
   */
  find(currentPage = 0, since = 0) {
    return new Promise((resolve) => {
      logger.debug(`@BING :: Processing page #${currentPage + 1}.`);

      // perform extraction until the page limit is reached
      if (currentPage < this.pages) {
        this._performRequest(this._formRequest(since))
          .then(html => this._processPage(html, currentPage))
          .then(data => {
            logger.info(`@BING :: ${data.results.length} links parsed from p.${currentPage}. ` +
              `More is about to come: ${data.moreToCome}`);

            _.map(data.results, dataElement => this.results.add(dataElement));

            // if there's no more pages to process, return results or
            // process the next page otherwise
            resolve(data.moreToCome ? this.find(++currentPage, since += 10) : [...this.results]);
          })
          .catch(error => {
            logger.warn(`@BING :: Links retrieval was interrupted -> ${error.message}.`);
            const leads = [...this.results];

            if (leads && !leads.length) {
              logger.warn(`@BING :: [${this.query} --${this.period}] -> **NO** leads to return.`);
              throw new Error('No leads retrieved');
            } else {
              resolve(leads);
            }
          });
      } else {
        const leads = [...this.results];

        if (leads && !leads.length) {
          logger.info(`@BING :: [${this.query} --${this.period}] -> **NO** leads retrieved`);
          throw new Error('No leads retrieved');
        } else {
          resolve(leads);
        }
      }
    });
  }
}

module.exports = BingSearch;
